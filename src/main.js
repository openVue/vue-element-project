// 在main.js的头部
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import * as echarts from 'echarts'
import './icons'
import './directives'
import '../mock'
import './styles/index.scss' // 全局样式
import './plugins' // 引入插件
import VAnimateCss from 'v-animate-css'
import dayjs from 'dayjs'
import print from './utils/print'
Vue.use(print)
Vue.use(VAnimateCss)
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.prototype.$dayjs = dayjs
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
