import Vue from 'vue'
import layout from '@/layouts'
import VueRouter from 'vue-router'
import demo from '@/views/demo'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'layout',
    component: layout,
    redirect: '/demo',
    children: [
      {
        path: '/demo',
        name: 'demo',
        component: demo
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes
})

export default router
